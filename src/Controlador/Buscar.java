package Controlador;

/**
 *
 * @author Leo
 */
public class Buscar {

    public int Metodo_Binario(String[][] g, int columna, String elemento) {
        int izq = 0, der = g.length - 1;
        while (izq <= der) {
            int indiceMedio = (int) Math.floor((izq + der) / 2);
            String[] elementoMedio = g[indiceMedio];
            int compara = elemento.compareTo(elementoMedio[columna]);

            if (compara == 0) {
                return indiceMedio;
            }
            if (compara < 0) {
                der = indiceMedio - 1;
            } else {
                izq = indiceMedio + 1;
            }
        }
        return -1;
    }

    public int Metodo_Secuencial(String[][] g, int valor, String element) {

        Integer pos = null;
        for (int b = 0; b <= g.length - 1; b++) {

            if (element.equals(g[b][valor])) {
                pos = b;
 
                break;
            }
        }
        return pos;
    }
    
}
