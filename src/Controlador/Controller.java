package Controlador;

import Controlador.tda.Lista.ListaEnlazadaServices;

import Modelo.DataE;
import Modelo.Gato;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Random;

/*
 * @author Leo
 */
public class Controller {

    private ListaEnlazadaServices<DataE> listaD = new ListaEnlazadaServices<>();
    private ListaEnlazadaServices<Gato> listaG = new ListaEnlazadaServices<>();
    private Gato gato;

    public void setListaG(ListaEnlazadaServices<Gato> listaG) {
        this.listaG = listaG;
    }

    public ListaEnlazadaServices<Gato> getListaGatos() {
        if (this.listaG == null) {
            this.listaG = new ListaEnlazadaServices<>();
        }
        return listaG;
    }

    public void setListaD(ListaEnlazadaServices<DataE> listaD) {
        this.listaD = listaD;
    }

    public void llenarLista() {
        DecimalFormat f = new DecimalFormat("#.00");
        Random r = new Random();

        for (int i = 0; i < 20000; i++) {
            DataE d = new DataE();

            float puntos = (float) r.nextFloat(100);
            float por = (float) r.nextFloat(100);

            d.setNumero(i + 1);
            d.setId((100 + r.nextFloat()) + (1000 + r.nextFloat(99999 - 10000)));
            d.setPuntos((Math.round(puntos * 100)) / 100);
            d.setPorcentaje((Math.round(por * 100)) / 100);
            listaD.insertarAlFinal(d);
        }
    }

    public void listaGatos() {

        Random r = new Random();

        String[] nombre = {"feliciano", "severino", "bonifacio", "emilio", "maximiliano", "sebastian", "leonidas"};
        String[] color = {"marron", "blanco", "negro", "naranja", "gris"};
        String[] desc = {"Patas largas", "Ojos claros", "Orejas cortas", "Cola larga", "Cola corta", "es diabolico"};

        try {
            
            for (int i = 0; i < 20; i++) {
                FileWriter fw = new FileWriter("Gatos.txt");
                Gato gato = new Gato();

                String name = nombre[r.nextInt(7)];
                String colour = color[r.nextInt(5)] + " y " + color[r.nextInt(5)];
                Integer age = r.nextInt(1, 25);
                Integer id = i + r.nextInt(100, 300);
                String d = desc[r.nextInt(6)];
                gato.setNombre(name);
                fw.write("\n\nNombre > " + nombre.toString());
                gato.setColor(colour);
                fw.write("\nColor > " + colour.toString());
                gato.setEdad(age);
                fw.write("\nEdad > " + age.toString());
                gato.setId(id);
                fw.write("\nID > " + id.toString());
                gato.setDescripcion(d.toString());
                fw.write("\nDetalle > " + d + "\n");
                listaG.insertarAlFinal(gato);
                fw.flush();
                fw.close();
            }
            
        } catch (Exception e) {
            System.out.println("no se puede guardar");

        }

    }

//    public void guardar(Gato cat) {
//        try {
//            FileWriter fw = new FileWriter("Gatos.txt");
//            for (int i = 0; i < 20; i++) {
//                
//                
//                
//                
//                fw.write("\nDetalle > " + cat.getDescripcion() + "\n");
//                fw.flush();
//                fw.close();
//            }
//        } catch (IOException e) {
//            System.out.println("no se puede guardar");
//        }
//    }
    public DataE[] toArray1() {
        DataE[] d = new DataE[listaD.getSize()];
        for (int i = 0; i < listaD.getSize(); i++) {
            d[i] = listaD.obtenerDato(i);
        }
        return d;
    }

    public Gato[] toArray2() {
        Gato[] g = new Gato[listaG.getSize()];
        for (int i = 0; i < listaG.getSize(); i++) {
            g[i] = listaG.obtenerDato(i);
        }
        return g;
    }

    public Gato getGato() {
        if (this.gato == null) {
            this.gato = new Gato();
        }
        return gato;
    }

    public void setGato(Gato gato) {
        this.gato = gato;
    }

//    public Boolean guardar() {
//        try {
//            getGato().setId(listaG.getSize()+1);
//            // guardar en archivo;
//        } catch (Exception e) {
//            System.out.println("Error en guardar gato" + e);
//
//        }
//        return false;
//    }
}
