
package Controlador.tda.Lista.Exception;

public class PosicionException extends Exception {

    public PosicionException() {
    }

    public PosicionException(String msg) {
        super(msg);
    }
}
