
package Vista.ModeloTabla;

import Controlador.tda.Lista.ListaEnlazadaServices;
import Modelo.Gato;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Leo
 */
public class TablaGatos extends AbstractTableModel{
    
    private ListaEnlazadaServices<Gato>listag;
    
    public ListaEnlazadaServices <Gato> getListaG(){
        return listag;
    }
    public void setListaG(ListaEnlazadaServices<Gato> listag){
        this.listag = listag;
    }
    @Override
    public int getRowCount() {
        return listag.getSize();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }
    @Override
    public String getColumnName(int column){
        return switch (column) {
            case 0 -> "Nro";
            case 1 -> "Nombre";
            case 2 -> "Tipo";
            case 3 -> "Edad";
            case 4 -> "Descripcion";
            default -> null;
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Gato E = listag.obtenerDato(rowIndex);
        return switch (columnIndex) {
            case 0 -> (E!=null)? E.getId(): "Sin definir";
            case 1 -> (E!= null)? E.getNombre(): "Sin definir";
            case 2 -> (E!= null) ? E.getColor(): "Sin definir";
            case 3 -> (E!=null) ? E.getEdad(): "Sin definir";
            case 4 -> (E!=null) ? E.getDescripcion(): "Sin definir";
            default -> null;
        };
    }
    
}
