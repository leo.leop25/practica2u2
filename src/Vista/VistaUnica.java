package Vista;

import Controlador.*;
import Modelo.*;
import Vista.ModeloTabla.TablaGatos;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Leo
 */
public class VistaUnica extends javax.swing.JDialog {

    /**
     * Creates new form VistaUnica
     */
    Controller control = new Controller();
    private TablaGatos t = new TablaGatos();

    //DataE[] orden = new DataE[20000];
    String tab1[][] = new String[20000][4];
    String tab2[][] = new String[20000][4];
    Integer n = 5;

    String tab3[][] = new String[20][5];
    String tab4[][] = new String[20][5];

    Integer fila = -1;

    public VistaUnica(java.awt.Frame parent, boolean modal) {

        super(parent, modal);
        initComponents();
        control.llenarLista();
        txtTiempo.setText("");
        TablaF1();

        control.listaGatos();
        mostrarGatos();
        //limpiar();
    }

    private void TablaF1() {
        DataE[] datos = new DataE[20000];
        datos = control.toArray1();
        String columnas[] = {"Numero", "ID", "Puntos", "Porcentaje"};

        for (int i = 0; i < tab1.length; i++) {
            tab1[i][0] = datos[i].getNumero() + "";
            tab1[i][1] = datos[i].getId() + "";
            tab1[i][2] = datos[i].getPuntos() + "";
            tab1[i][3] = datos[i].getPorcentaje() + "%";
        }
        tablaEj1.setModel(new DefaultTableModel(tab1, columnas));
    }

    private void TablaF2() {
        DataE[] dat = new DataE[20000];
        String columnas[] = {"Numero", "ID", "Puntos", "Porcentaje"};
        tablaEj1.setModel(new DefaultTableModel(tab1, columnas));
    }

    private void mostrarGatos() {
        Gato[] gato = new Gato[20];
        gato = control.toArray2();
        String columnas[] = {"ID", "Nombre", "Color", "Edad", "Detalle"};

        for (int i = 0; i < tab3.length; i++) {

            tab3[i][0] = gato[i].getId() + "";
            tab3[i][1] = gato[i].getNombre() + "";
            tab3[i][2] = gato[i].getColor() + "";
            tab3[i][3] = gato[i].getEdad() + "";
            tab3[i][4] = gato[i].getDescripcion() + "";
        }
        tablaEj2.setModel(new DefaultTableModel(tab3, columnas));
    }

    private void OrdenGato() {
        Gato[] gato = new Gato[20];
        String columnas[] = {"ID", "Nombre", "Color", "Edad", "Descripcion"};
        tablaEj2.setModel(new DefaultTableModel(tab3, columnas));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaEj1 = new javax.swing.JTable();
        cbxMetodo = new javax.swing.JComboBox<>();
        cbxOrden = new javax.swing.JComboBox<>();
        cbxAtributo = new javax.swing.JComboBox<>();
        btonOrdenar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtTiempo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaEj2 = new javax.swing.JTable();
        txtElemento = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cbxBuscarAt = new javax.swing.JComboBox<>();
        btonBuscar = new javax.swing.JButton();
        txtFound = new javax.swing.JLabel();
        cbxMetodoOr = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tablaEj1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaEj1);

        cbxMetodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Shell", "Quicksort" }));

        cbxOrden.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ascend.", "Descent" }));

        cbxAtributo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Puntos", "Porcentaje" }));

        btonOrdenar.setText("Ordenar");
        btonOrdenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonOrdenarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel1.setText("EJERCICIO 1");

        txtTiempo.setText(".");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(141, 141, 141))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cbxMetodo, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cbxAtributo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbxOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(btonOrdenar)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(txtTiempo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxMetodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxAtributo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtTiempo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(btonOrdenar)))
                .addContainerGap(101, Short.MAX_VALUE))
        );

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tablaEj2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tablaEj2);

        jLabel6.setText("Elemento:");

        cbxBuscarAt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Nombre", "Color", "Edad" }));
        cbxBuscarAt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxBuscarAtActionPerformed(evt);
            }
        });

        btonBuscar.setText("Buscar");
        btonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonBuscarActionPerformed(evt);
            }
        });

        txtFound.setText(".");

        cbxMetodoOr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Binario", "Secuencial" }));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel2.setText("EJERCICIO 2");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtElemento, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(cbxBuscarAt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbxMetodoOr, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(179, 179, 179)
                        .addComponent(btonBuscar))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtFound, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(12, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(170, 170, 170))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtFound)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtElemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbxBuscarAt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbxMetodoOr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btonBuscar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void ordenar() {
        Ordenar orden = new Ordenar();
        long start = System.nanoTime();

        if (cbxMetodo.getSelectedIndex() == 0) {
            tab2 = orden.Shell(tab1, cbxAtributo.getSelectedIndex() + 1, ascender());

        } else {
            tab2 = orden.Quicksort(tab1, cbxAtributo.getSelectedIndex() + 1, 0, tab1.length - 1);

            if (!ascender()) {
                tab2 = orden.invertir(tab2);
            }

        }
        long end = System.nanoTime() - start;
        txtTiempo.setText("Tiempo de metodo " + cbxMetodo.getSelectedItem().toString() + " es " + end / 1e6 + " ms.");
        TablaF2();

    }

    private void buscar() {
        Ordenar orden = new Ordenar();
        Buscar buscar = new Buscar();
        OrdenGato();

        if (cbxMetodoOr.getSelectedIndex() == 0) {
            tab4 = orden.Shell(tab3, cbxBuscarAt.getSelectedIndex(), true);

            int in = buscar.Metodo_Binario(tab4, cbxBuscarAt.getSelectedIndex(), txtElemento.getText());
            if (in <= 0) {
                txtFound.setText("Sin Resultados.");
            } else {

                txtFound.setText("Encontrado como  " + ("Nombre: " + tab4[in][1] + " Edad:" + tab4[in][3]
                        + " Color:" + tab4[in][2]));
            }
        } else {
            int in = buscar.Metodo_Secuencial(tab4, cbxBuscarAt.getSelectedIndex(), txtElemento.getText());
            if (in <= 0) {
                txtFound.setText("Sin Resultados.");
            } else {

                txtFound.setText("Encontrado como  " + ("Nombre: " + tab4[in][1] + " Edad:" + tab4[in][3]
                        + " Color:" + tab4[in][2]));
            }
        }

        //tab4 = orden.Shell(tab4, 0, true);
        //int in = buscar.Metodo_Binario(tab4, 0, txtElemento.getText());
    }

    private boolean ascender() {
        if (cbxOrden.getSelectedIndex() == 0) {
            return true;
        } else {
            return false;
        }
    }

//    private void guardar() {
//        if (txtNombre.getText().isEmpty() || txtEdad.getText().isEmpty() || txtTipo.getText().isEmpty() || txtDes.getText().isEmpty()) {
//            JOptionPane.showMessageDialog(null, "Llenar todos los campos correspondientes");
//        } else {
//            control.getGato().setNombre(txtNombre.getText());
//            control.getGato().setEdad(Integer.parseInt(txtEdad.getText()));
//            control.getGato().setColor(txtTipo.getText());
//            control.getGato().setDescripcion(txtDes.getText());
//
//        }
//        if (fila == -1) {
//            if (control.getListaGatos().insertarAlFinal(control.getGato())) {
//                System.out.println("Se ha guardardo el gato(:");
//                if (control.guardar()) {
//                    System.out.println("se guardo json");
//                } else {
//                    System.out.println(":)");
//                }
//                //mostrarGatos();
//                limpiar();
//
//            } else {
//                JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
//            }
//
//        } else {
//            if (control.getListaGatos().modificarDatoPosicion(fila, control.getGato())) {
//                JOptionPane.showMessageDialog(null, "Se ha modificado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
//                limpiar();
//            } else {
//                JOptionPane.showMessageDialog(null, "No se pudo modificar", "Error", JOptionPane.ERROR_MESSAGE);
//            }
//        }
//
//    }

    private void btonOrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonOrdenarActionPerformed
        // TODO add your handling code here:
        ordenar();
    }//GEN-LAST:event_btonOrdenarActionPerformed

    private void cbxBuscarAtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxBuscarAtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxBuscarAtActionPerformed

    private void btonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonBuscarActionPerformed
        // TODO add your handling code here:
        buscar();

    }//GEN-LAST:event_btonBuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaUnica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaUnica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaUnica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaUnica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaUnica dialog = new VistaUnica(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btonBuscar;
    private javax.swing.JButton btonOrdenar;
    private javax.swing.JComboBox<String> cbxAtributo;
    private javax.swing.JComboBox<String> cbxBuscarAt;
    private javax.swing.JComboBox<String> cbxMetodo;
    private javax.swing.JComboBox<String> cbxMetodoOr;
    private javax.swing.JComboBox<String> cbxOrden;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablaEj1;
    private javax.swing.JTable tablaEj2;
    private javax.swing.JTextField txtElemento;
    private javax.swing.JLabel txtFound;
    private javax.swing.JLabel txtTiempo;
    // End of variables declaration//GEN-END:variables
}
